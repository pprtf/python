import java.util.Scanner;

public class Main
{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
       // String input = ")))()))()()()(((()))()))()((()(((";
        String[] pSeq = input.split("");
        String[] tmp  = new String[pSeq.length];
        Val val;
        
        for (int i=0;i<pSeq.length;i++) {
            if (pSeq[i].equals("(")){
                val = next(i,pSeq);
                if (val.getValue().equals(")")) {
                    tmp[i]=pSeq[i];
                    tmp[val.getId()]=pSeq[val.getId()];
                    pSeq[i]="0";
                    pSeq[val.getId()]="0";
                }
            }
            if (pSeq[i].equals(")")){
                val = prev(i,pSeq);
                if (val.getValue().equals("(")) {
                    tmp[i]=pSeq[i];
                    tmp[val.getId()]=pSeq[val.getId()];
                    pSeq[i]="0";
                    pSeq[val.getId()]="0";
                }
            }
            
        }
        
        int counter = 0;
        String seq = "";
        for (int i=0;i<tmp.length;i++)
            if (tmp[i]!=null && !tmp[i].equals("0")){
                seq+=tmp[i];
                counter++;
            }
        System.out.println(counter+" - "+seq);
    }
    
    static Val next(int i, String[] pSeq) {
        for (int j=i+1;j<pSeq.length;j++)
            if (!pSeq[j].equals("0")) 
                return new Val(j,pSeq[j]);
        return new Val(0,"0");
    }
    
    static Val prev(int i, String[] pSeq) {
        for (int j=i-1;j>-1;j--)
            if (!pSeq[j].equals("0"))
                return new Val(j,pSeq[j]);
        return new Val(0,"0");
    }
}

class Val {
    int id;
    String value;
    
    public Val() {
        
    }
    
    public Val(int id, String value) {
        this.id = id;
        this.value = value;
    }
    
    public int getId() {
        return this.id;
    }
    
    public String getValue() {
        return this.value;
    }
}