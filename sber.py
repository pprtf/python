#https://colab.research.google.com/drive/1wsykqsUrJmsJRkQBFQNbrmrVHZ0_xZae?usp=sharing
#Функции для определения следующего и предыдущего символа и его индекса
def next1(i):
  for j in range(i+1,len(a)):
    if a[j] != 0:
      return j,a[j]  

def prev(i):
  for j in range(i-1,-1,-1):
    if a[j] != 0:
      return j,a[j] 
  
#Ввод последовательности. Предполагается, что она всегда верна.
#Нули - заглушки, чтобы не выходить за границы массива, и не писать лишние условия
#Ввод распарсим в массив
print("Input parentheses seq")
a = list("0"+input()+"0")
tmp = [0]*len(a) #Массив, куда будем складировать получившиеся результаты
#Здесь бежим по массиву и ищем для ближайшей открытой скобки закрытую впереди, а для закрытой
#открытую позади. Когда совпало, добавляем в тмп массив на те же индексы, а сами элементы зануляем
for i in range(0,len(a)):
  if a[i]=="(":
    ni, nai = next1(i)
    if nai == ")":
      tmp[i]=a[i]
      tmp[ni]=a[ni]      
      a[i]=0
      a[ni]=0
  if a[i]==")":
    pi, pai = prev(i)
    if pai == "(":
      tmp[i]=a[i]
      tmp[pi]=a[pi]      
      a[i]=0
      a[pi]=0

#Массив тмп приведем в строку и удалим нули
a = ''.join(str(x) for x in tmp).replace('0',"")
print(f"{len(a)} - {a}")